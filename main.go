package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"
	"strings"
	"bytes"
	"io/ioutil"
	"html/template"
	"github.com/coreos/go-oidc/oidc"
	"github.com/coreos/go-oidc/oauth2"
	"github.com/coreos/go-oidc/jose"
	"github.com/nu7hatch/gouuid"

)

var (
	pathCallback      = "/oauth2callback"
	defaultListenHost = "127.0.0.1:5555"
)

func main() {
	log.SetOutput(os.Stderr)

	fs := flag.NewFlagSet("identity-client-registrator", flag.ExitOnError)
	listen := fs.String("listen", defaultListenHost, "serve traffic on this address (<host>:<port>)")
	redirectURL := fs.String("redirect-url", fmt.Sprintf("http://%s%s", defaultListenHost, pathCallback), "")
	clientID := fs.String("client-id", "", "")
	clientSecret := fs.String("client-secret", "", "")
	discovery := fs.String("discovery", "https://id-test.teliasonera.no/auth/realms/master", "")

	if err := fs.Parse(os.Args[1:]); err != nil {
		log.Fatalf("failed parsing flags: %v", err)
	}

	if *clientID == "" {
		log.Fatal("--client-id must be set")
	}

	if *clientSecret == "" {
		log.Fatal("--client-secret must be set")
	}

	_, _, err := net.SplitHostPort(*listen)
	if err != nil {
		log.Fatalf("unable to parse host:port from --listen flag: %v", err)
	}

	cc := oidc.ClientCredentials{
		ID:     *clientID,
		Secret: *clientSecret,
	}

	log.Printf("fetching provider config from %s...", *discovery)

	var cfg oidc.ProviderConfig
	for {
		cfg, err = oidc.FetchProviderConfig(http.DefaultClient, *discovery)
		if err == nil {
			break
		}

		sleep := 3 * time.Second
		log.Printf("failed fetching provider config, trying again in %v: %v", sleep, err)
		time.Sleep(sleep)
	}

	log.Printf("fetched provider config from %s: %#v", *discovery, cfg)

	ccfg := oidc.ClientConfig{
		ProviderConfig: cfg,
		Credentials:    cc,
		RedirectURL:    *redirectURL,
	}

	client, err := oidc.NewClient(ccfg)
	if err != nil {
		log.Fatalf("unable to create Client: %v", err)
	}

	client.SyncProviderConfig(*discovery)

	redirectURLParsed, err := url.Parse(*redirectURL)
	if err != nil {
		log.Fatalf("unable to parse url from --redirect-url flag: %v", err)
	}

	hdlr := NewClientHandler(client, *redirectURLParsed)
	httpsrv := &http.Server{
		Addr:    fmt.Sprintf(*listen),
		Handler: hdlr,
	}

	log.Printf("binding to %s...", httpsrv.Addr)
	log.Fatal(httpsrv.ListenAndServe())
}

func NewClientHandler(c *oidc.Client, cbURL url.URL) http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/", handleIndex)
	mux.HandleFunc("/login", handleLoginFunc(c))
	mux.HandleFunc("/registrationForm", handleRegistrationFormFunc(c))
	mux.HandleFunc("/create", handleCreateFunc(c))
	mux.HandleFunc(pathCallback, handleCallbackFunc(c))
	return mux
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	filename := r.URL.Path[1:]

	if filename == "" {
    	filename = "index.html"
    }

    body, err := ioutil.ReadFile(filename)
    
    if strings.HasSuffix(filename, "css") {
		w.Header().Set("Content-Type", "text/css")
    }

    if err != nil {
        w.WriteHeader(http.StatusNotFound)
    } else {
    	fmt.Fprintf(w, "%s", body)
	}
}

func handleLoginFunc(c *oidc.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		oac, err := c.OAuthClient()
		if err != nil {
			panic("unable to proceed")
		}

		u, err := url.Parse(oac.AuthCodeURL("", "", ""))
		if err != nil {
			panic("unable to proceed")
		}
		http.Redirect(w, r, u.String(), http.StatusFound)
	}
}

type KeycloakClientRepresentation struct {
	Id             string `json:"id"`
	Name           string `json:"name"`
	ClientId       string `json:"clientId"`
	Secret         string `json:"secret"`
	RedirectURIs []string `json:"redirectUris"`
	PublicClient     bool `json:"publicClient"`
	WebOrigins   []string `json:"webOrigins"`
}


func handleCreateFunc(c *oidc.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		r.ParseForm()
        fmt.Println(r.Form)
        fmt.Println("path", r.URL.Path)
	    fmt.Println("scheme", r.URL.Scheme)
	    fmt.Println(r.Form["url_long"])
	    for k, v := range r.Form {
	        fmt.Println("key:", k)
	        fmt.Println("val:", strings.Join(v, ""))
	    }

		secret, err := uuid.NewV4()
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("Failed to generate secret: %v", err))
			return
		}

		kc := &KeycloakClientRepresentation {
			Id:          "19999999999999",
			Name:        strings.Join(r.Form["Name"], ""),		
			ClientId:    strings.Join(r.Form["ClientId"], ""),  
			Secret:      secret.String(),						
			RedirectURIs: r.Form["RedirectURIs"],				
			PublicClient: false,
			WebOrigins:   []string{"*"},
		}

	    payload, err := json.Marshal(kc)
	    if err != nil {
	        fmt.Println(err)
	        return
	    }
	    fmt.Println(string(payload))

		accessCookie, err  := r.Cookie("jwtAccess")
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("AccessToken Cookie error: %v", err))
			return
		}

		/*accessToken, err := jose.ParseJWT(accessCookie.Value)
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to parse AccessToken: %v", err))
			return
		}*/

		url := "https://id-test.teliasonera.no/auth/admin/realms/telia/clients"
 		req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
   		req.Header.Set("Authorization", "Bearer " + accessCookie.Value)
   		req.Header.Set("Accept", "application/json, text/plain, */*")
   		req.Header.Set("Content-Type", "application/json")

   		fmt.Println("request headers: ", req.Header)

	  	client := &http.Client{}
	    resp, err := client.Do(req)
	    if err != nil {
	        panic(err)
	    }
	    defer resp.Body.Close()

	    fmt.Println("response Status:", resp.Status)
	    fmt.Println("response Headers:", resp.Header)
	    body, _ := ioutil.ReadAll(resp.Body)
	    fmt.Println("response Body:", string(body))

		//refreshCookie, _ := r.Cookie("jwtRefresh")
		/*refreshToken, err := jose.ParseJWT(refreshCookie.Value)
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to parse IDToken: %v", err))
			return
		}*/

	    t, _ := template.ParseFiles("create.html")
	    t.Execute(w, kc)
	}
}

func handleRegistrationFormFunc(c *oidc.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		accessCookie, err  := r.Cookie("jwtAccess")
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("AccessToken Cookie error: %v", err))
			return
		}

		refreshCookie, _ := r.Cookie("jwtRefresh")
		idCookie, _      := r.Cookie("jwtID")

		idToken, err := jose.ParseJWT(idCookie.Value)
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to parse IDToken: %v", err))
			return
		}

		accessToken, err := jose.ParseJWT(accessCookie.Value)
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to parse AccessToken: %v", err))
			return
		}

		claims, err := idToken.Claims()
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to construct claims: %v", err))
			return
		}

		accessClaims, err := accessToken.Claims()
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to construct claims: %v", err))
			return
		}

		email, has_email, err := claims.StringClaim("email")
		if err != nil || !has_email {
			email = "undefined"
		}

		s := fmt.Sprintf("claims: %v", claims)
		ac := fmt.Sprintf("claims: %v", accessClaims)

		u := &User{Name: email, AccessToken: ac, Claims: s, RefreshToken: refreshCookie.Value}
	    t, _ := template.ParseFiles("registrationForm.html")
	    t.Execute(w, u)
	}
}

type User struct {
	Name string
	Claims string
	AccessToken string
	RefreshToken string
}

func handleCallbackFunc(c *oidc.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		code := r.URL.Query().Get("code")
		if code == "" {
			writeError(w, http.StatusBadRequest, "code query param must be set")
			return
		}

		oac, err := c.OAuthClient()
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to get OAuthClient: %v", err))
		}

		tokenResponse, err := oac.RequestToken(oauth2.GrantTypeAuthCode, code)
		if err != nil {
			writeError(w, http.StatusBadRequest, fmt.Sprintf("unable to verify auth code with issuer: %v", err))
		}

		expiration := time.Now().Add(time.Hour)
		accessCookie := http.Cookie{Name: "jwtAccess", Value: tokenResponse.AccessToken, Expires: expiration}
		refreshCookie := http.Cookie {Name: "jwtRefresh", Value: tokenResponse.RefreshToken, Expires: expiration}
		idCookie := http.Cookie{Name: "jwtID", Value: tokenResponse.IDToken, Expires: expiration}

		http.SetCookie(w, &accessCookie)
		http.SetCookie(w, &refreshCookie)
		http.SetCookie(w, &idCookie)
		http.Redirect(w, r, "/registrationForm", http.StatusFound)
	}
}

func writeError(w http.ResponseWriter, code int, msg string) {
	e := struct {
		Error string `json:"error"`
	}{
		Error: msg,
	}
	b, err := json.Marshal(e)
	if err != nil {
		log.Printf("Failed marshaling %#v to JSON: %v", e, err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(b)
}
